class RPNCalculator

  attr_accessor :value, :stack

  def initialize
    @value = 0
    @stack = []
  end

  def push(num)
    self.stack.push(num)
  end

  def plus
    check_empty
    if self.stack.length > 1
      self.value = self.stack[-2] + self.stack[-1]
    else
      self.value += self.stack[0]
    end
    self.stack.pop
    self.stack.pop
  end

  def minus
    check_empty
    if self.stack.length > 1
      self.value = self.stack[-2] - self.stack[-1]
    else
      self.value -= self.stack[0]
    end
    self.stack.pop
    self.stack.pop
  end

  def times
    check_empty
    if self.stack.length > 1
      self.value = self.stack[-2] * self.stack[-1]
    else
      self.value *= self.stack[0]
    end
    self.stack.pop
    self.stack.pop
  end

  def divide
    check_empty
    if self.stack.length > 1
      self.value = self.stack[-2].to_f / self.stack[-1].to_f
    else
      self.value /= self.stack[0]
    end
    self.stack.pop
    self.stack.pop
  end

  def check_empty
    if self.stack.empty?
      raise 'calculator is empty'
    end
  end

end
